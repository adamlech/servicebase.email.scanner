package servicebase.email.scanner.dao;

import java.io.FileWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import servicebase.email.scanner.model.SomsRequest;
import servicebase.email.scanner.model.SomsResponse;

@Component
public class SomsEtlDao {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	private String somsEtlUrl;

	private FileWriter fileWriter;

	@Value("${soms.etl.url}/api/soms/pull")
	public void setSomsEtlUrl(String somsEtlUrl) {
		this.somsEtlUrl = somsEtlUrl;
	}

	public void setFileWriter(FileWriter fileWriter) {
		this.fileWriter = fileWriter;
	}

	public ResponseEntity<SomsResponse> somsPull(SomsRequest request) {

		RestTemplate restTemplate = new RestTemplate();

		try {
			ResponseEntity<SomsResponse> response = restTemplate.postForEntity(this.somsEtlUrl, request,
					SomsResponse.class);

			SomsResponse somsResponse = response.getBody();

			if (somsResponse.isSuccess()) {
				logger.info("Order number: {} for account: {} in library : {} was created", request.getOrderNumber(),
						request.getAccountNumber(), request.getLibraryName());
				String row = this.buildRow(request.getOrderNumber(), request.getAccountNumber(),
						request.getLibraryName(), "OK", "SUCCESS");
				fileWriter.write(row);

			}

			else {

				logger.error("\"Order number: {} for account: {} in library : {} was not created because: {}",
						request.getOrderNumber(), request.getAccountNumber(), request.getLibraryName(),
						somsResponse.getError());
				String row = this.buildRow(request.getOrderNumber(), request.getAccountNumber(),
						request.getLibraryName(), "FAIL", somsResponse.getError());
				fileWriter.write(row);
			}
			return response;
		} catch (Exception e) {
			SomsResponse somsResponse = new SomsResponse();
			somsResponse.setError(e.getMessage());
			somsResponse.setSuccess(false);
			ResponseEntity<SomsResponse> response = new ResponseEntity<SomsResponse>(somsResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error("There was an exception when calling soms pull request {}", e.getMessage(), e);
			return response;

		}

	}

	private String buildRow(String orderNumber, String accountNumber, String library, String status, String message) {

		StringBuilder bld = new StringBuilder();
		bld.append(orderNumber);
		bld.append(",");
		bld.append(accountNumber);
		bld.append(",");
		bld.append(library);
		bld.append(",");
		bld.append(status);
		bld.append(",");
		bld.append(message);
		bld.append("\n");

		return bld.toString();

	}

}
