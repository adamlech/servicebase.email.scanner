package servicebase.email.scanner;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.PropertySource;

import servicebase.email.scanner.resources.EmailReceiver;
import servicebase.email.scanner.service.SomsEtlService;

@SpringBootApplication
@PropertySource("file:/etc/comporium/servicebase.order.scanner/application.properties")
public class Application {

	public static void main(String[] args) {

		ConfigurableApplicationContext ctx = SpringApplication.run(Application.class, args);

		EmailReceiver emailReceiver = ctx.getBean(EmailReceiver.class);
		SomsEtlService service = ctx.getBean(SomsEtlService.class);

		List<String> messages = emailReceiver.getALlEmailsFromFieldServices();
		System.out.println(messages.size());

		String outputPath = ctx.getEnvironment().getProperty("results.output.path");
		FileWriter fileWriter;
		try {
			outputPath = addDateToOutputPath(outputPath); 
			fileWriter = new FileWriter(outputPath);
			fileWriter.write(buildHeader());
			service.setFileWirter(fileWriter);

			messages.forEach(emailBody -> {
				service.parsAndPullSoms(emailBody);
			});
			fileWriter.close();
		} catch (IOException e) {

			System.out.println("Error setting up output file");
		}

		System.exit(0);
	}

	private static String buildHeader() {
		StringBuilder bld = new StringBuilder(); 
		bld.append("Order Number"); 
		bld.append(",");
		bld.append("AccountNumber");
		bld.append(","); 
		bld.append("Library");
		bld.append(",");
		bld.append("Status");
		bld.append(","); 
		bld.append("Message");
		bld.append("\n");
		

		return bld.toString();
	}
	
	private static String addDateToOutputPath(String outputPath) {
		
		String[] array = outputPath.split(".csv");
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyMMddhhmmss");
	    Date date = new Date(); 
	    String dateString = dateFormat.format(date);	    
	    String finalOutputPath  = String.format("%s-%s.csv", array[0] , dateString);
	    return finalOutputPath; 
	}
}
