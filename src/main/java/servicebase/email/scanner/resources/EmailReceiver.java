package servicebase.email.scanner.resources;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.ComparisonTerm;
import javax.mail.search.ReceivedDateTerm;
import javax.mail.search.SearchTerm;
import javax.print.attribute.standard.DateTimeAtProcessing;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EmailReceiver {

	
	Logger logger = LoggerFactory.getLogger(this.getClass()); 
	
	
	private String outlookUsername;
	
	private String outlookPassword; 
	
	private String outlookFolder; 
	
	@Value("${outlook.mail.user}")
	public void setOutlookUsername(String outlookUsername) {
		this.outlookUsername = outlookUsername;
	}

	@Value("${outlook.mail.password}")
	public void setOutlookpassword(String outlookpassword) {
		this.outlookPassword = outlookpassword;
	}

	@Value("${outlook.mail.folder}")
	public void setOutlookFolder(String outlookFolder) {
		this.outlookFolder = outlookFolder;
	}

	public List<String> getALlEmailsFromFieldServices() {
	
		Properties props = new Properties();
		props.setProperty("mail.imap.host", "outlook.office365.com");
		props.setProperty("mail.imap.port", "993");
		props.setProperty("mail.pop3.starttls.enable", "true");
		Session emailSession = Session.getDefaultInstance(props);

		// create the POP3 store object and connect with the pop server
 
		List<String> emailList = new LinkedList<>(); 
		try {

			Store store = emailSession.getStore("imaps");
			store.connect("outlook.office365.com", this.outlookUsername, this.outlookPassword);

			// create the folder object and open it
			Folder emailFolder = store.getFolder(this.outlookFolder);
			emailFolder.open(Folder.READ_ONLY);

			// retrieve the messages from the folder in an array and print it
			Calendar cal = Calendar.getInstance(); 
			cal.set(2019, 1, 4, 1, 0, 0);
			Date date = new Date(cal.getTimeInMillis()); 
			
			SearchTerm term = new ReceivedDateTerm(ComparisonTerm.GT, date); 
			Message[] messages = emailFolder.search(term);
			System.out.println("messages.length---" + messages.length);

			for (int i = 0, n = messages.length ; i < n; i++) {
				Message message = messages[i];
			    System.out.println(message.getReceivedDate());
				String result = "";
			    if (message.isMimeType("text/plain")) {
			        result = message.getContent().toString();
			    }
			    else if(message.isMimeType("text/html")) {
			    	result = this.br2nl(message.getContent().toString()); 
			    }
			    else if (message.isMimeType("multipart/*")) {
			        MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
			        result = getTextFromMimeMultipart(mimeMultipart);
			    }
			    else {
			    	;
			    }
			    logger.info("Message {} sent on {} was added" , message.getSubject() , message.getSentDate());
				emailList.add(result); 

			}

			// close the store and folder objects
			emailFolder.close(false);
			store.close();

		} catch (MessagingException | IOException e) {
			
			logger.error("Problem receiving messages: {}" , e.getMessage(), e);
		}
		
		return emailList; 
	}

	private  String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws MessagingException, IOException {
		String result = "";
		int count = mimeMultipart.getCount();
		for (int i = 0; i < count; i++) {
			BodyPart bodyPart = mimeMultipart.getBodyPart(i);
			if (bodyPart.isMimeType("text/plain")) {
				result = result + "\n" + bodyPart.getContent();
				break; // without break same text appears twice in my tests
			} else if (bodyPart.isMimeType("text/html")) {
				String html = (String) bodyPart.getContent();
				result = result + br2nl(html);
			} else if (bodyPart.getContent() instanceof MimeMultipart) {
				result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
			}
		}
		return result;
	}

	private String br2nl(String html) {
		if (html == null)
			return html;
		Document document = Jsoup.parse(html);
		document.outputSettings(new Document.OutputSettings().prettyPrint(false));
		document.select("br").append("\\n");
		document.select("p").prepend("\\n\\n");
		String s = document.html().replaceAll("\\\\n", "\n");
		return Jsoup.clean(s, "", Whitelist.none(), new Document.OutputSettings().prettyPrint(false));
	}
}
