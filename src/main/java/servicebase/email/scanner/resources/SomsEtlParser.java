package servicebase.email.scanner.resources;

import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import servicebase.email.scanner.model.SomsRequest;

@Component
public class SomsEtlParser {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public SomsRequest parseEmailBody(String body) {
		
		logger.info("Following email body was received for parasing: {}" , body); 
		
		Scanner scanner = new Scanner(body); 
		SomsRequest request = new SomsRequest(); 
		while(scanner.hasNextLine()) {
		
			
			String line = scanner.nextLine(); 
			if(line.startsWith("Order:")) {
				String[] array = line.split(": ");
				request.setOrderNumber(array[1]);
			}
			if(line.startsWith("Account:")) {
				String[] array = line.split(": ");
				request.setAccountNumber(array[1]);
			}
			if(line.startsWith("Library:")) {
				String[] array = line.split(": ");
				request.setLibraryName(array[1] != null ? array[1].trim() : "");
			}
			
			
		}
		request.setAction("CREATE");
		
		scanner.close();
		return request; 
		
		
	}
}
