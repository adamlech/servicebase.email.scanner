package servicebase.email.scanner.service;

import java.io.FileWriter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import servicebase.email.scanner.dao.SomsEtlDao;
import servicebase.email.scanner.model.SomsRequest;
import servicebase.email.scanner.model.SomsResponse;
import servicebase.email.scanner.resources.SomsEtlParser;

@Component
public class SomsEtlService {

	private SomsEtlDao somsEtlDao; 
	
	private SomsEtlParser parser; 
	
	@Autowired
	public SomsEtlService(SomsEtlDao somsEtlDao, SomsEtlParser parser) {
	
		this.somsEtlDao = somsEtlDao;
		this.parser = parser;
	}
	
	public void setFileWirter(FileWriter fileWriter) {
		this.somsEtlDao.setFileWriter(fileWriter);
	}
	
	public ResponseEntity<SomsResponse> parsAndPullSoms(String emailBody) {
		SomsRequest request = this.parser.parseEmailBody(emailBody); 
		return somsEtlDao.somsPull(request); 
	}
}
