package servciebase.email.scanner.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import servicebase.email.scanner.model.SomsResponse;
import servicebase.email.scanner.service.SomsEtlService;

@RestController
public class SomsEtlController {

	private SomsEtlService somsEtlService; 
	
	@Autowired
	public SomsEtlController(SomsEtlService somsEtlService) {
		super();
		this.somsEtlService = somsEtlService;
	}


	@PostMapping("/api/pullsoms")
	public ResponseEntity<SomsResponse> pullSoms(@RequestBody String request){
		
		return somsEtlService.parsAndPullSoms(request); 
	}
}
